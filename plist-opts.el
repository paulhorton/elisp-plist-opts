;;; plist-opts.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.
;; License: This software may be used and/or distributed under the GNU General Public License
;; Contact:  ,(concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210210
;; Updated: 20210210
;; Version: 0.0
;; Keywords: keyword arguments

;;; Commentary:

;; Functions for working on named options stored in a plist, cl-defun style
;; (:key1 value1 key2 value2...)
;;
;; Created because I found the cl-defmacro &key mechanism is not so convenient
;; for forwarding some named options from function A to function C via function B
;;

;;; Change Log:

;;; Code:


(require 'shortcuts)



(defmacro plist-opts/let (plo opt-key-list body)
  "
Unimplemented stub.

Instead of hand-coded let block
 (let (; options
       (skip-re  (or (plist-opts/remove PLO :skip-re)) \"#\")
       (keep-len     (plist-opts/remove PLO :keep-len))
       )
       BODY...
 )

Use:
 (plist-opts/let PLO
   '((:skip-re \"#\") :keep-len)
   BODY
"
  (declare (indent 3))
  (ignore plo opt-key-list body)
  ;... --->  insert masterful code here <---
  ; alist-let pry can give good hints.
)


(cl-defun plist-opts/key-sans: (opt-key &optional (msg "plist-opts/key-sans: "))
  "Return interned symbol with name equal to OPT-KEY without the leading ':'
For example, :test  --->  test"
  ;; This function was defined with the idea that it could be a small part of
  ;; the currently (202102) unimplemented plist-opts/let macro.
  (if (symbolp opt-key)
      (let1  opt-key-name  (symbol-name opt-key)
        (if (eq ?: (aref opt-key-name 0))
            (intern (substring opt-key-name 1))
          (error "%sputative opt-key symbol name \"%s\" does not start with ':'" msg opt-key-name)
          )
        )
    (error "%s putative opt-key {%S} not a symbol" msg opt-key)
    ))



(defmacro plist-opts/strip-extra-paren (plo)
  "If PLO has an extra pair of outer parens;
strip that pair from PLO (modifying PLO!)

Use to recover original opts after passing as arg to function taking &rest arg
Example.  foo and goo are both declared

  (defun foo (&rest opts)
     (goo opts))
  (defun goo (&rest opts)
     ...)

Now compare (goo :temp 78) to (foo :temp 78)

foo simply forwards opts, but because of the &rest declaration
when forwarded via foo, goo will receive '((:temp 78)) with
an extra pair of outer parens.

One way to avoid this problem would be to define foo as
  (defun foo (&rest opts)
     (apply 'goo opts)
  )

If, however for some reason that is not convenient,
you can use this function plist-opts/strip-extra-paren
To avoid does with the problem in goo, without changing foo.

  (defun goo (&rest opts)
     ;; here opts might be '(:temp 78) or '((:temp 78))
     (plist-opts/strip-extra-paren opts)
     ;; here opts is '(:temp 78)
      ...)

Defined as macro to allow it to propagate changes to the caller.
"
  `(when (and  (consp ,plo)  (listp (car ,plo)))
     (when (cdr-safe ,plo)
       (error " plist-opts; strip-extra-paren recieved unexpected form arg:  %S" ,plo))
     (setq ,plo (car ,plo))
     ));  strip extraneous outer paren pair


(defmacro plist-opts/remove (plo key)
  "If key KEY appears in plist PLO, remove all of those pairs and
return the value of the first pair
otherwise return nil"
  ;; Defined as macro instead of function, so that when (setcar PL) is used
  ;; to remove the head of list PL, this change will propagate back to the caller.
  (let ((retval (cl-gensym)))
    `(when
         ,plo
       (plist-opts/assert-well-formed ,plo "Called from plist-opts/remove; ")
       (let (,retval)
         (if (eq ,key (car ,plo))
             (progn
               (setq ,retval (cadr ,plo))
               (plist-opts/remove--except-head--no-return ,plo ,key)
               (setq ,plo (cddr ,plo))
               ,retval
               )
           (car (last
                 (plist-opts/remove--except-head ,plo ,key)
                 ))
           )))))


(defun plist-opts/remove--except-head (plo key)
  "Auxillary function for plist-opts/remove.
Assume PLO is a property list holding at least one pair.
This function leaves that first pair unchanged (even if its key eqs KEY).
But destructively removes any occurences of other pairs matching KEY.
Returns list of the values of removed pairs"
  (let* ((prv-pair-val-cell (cdr plo))
         (cur-pair-key-cell (cdr prv-pair-val-cell))
         vals
        )
    (while (consp (cdr cur-pair-key-cell))
      (if (eq key (car cur-pair-key-cell))
          (progn
            (push (cadr cur-pair-key-cell) vals)
            (setq vals (cadr cur-pair-key-cell))
            (setq cur-pair-key-cell (cddr cur-pair-key-cell))
            (setcdr prv-pair-val-cell cur-pair-key-cell)
            )
        (setq cur-pair-key-cell (cddr cur-pair-key-cell))
        (setq prv-pair-val-cell (cddr prv-pair-val-cell)
              cur-pair-key-cell (cdr prv-pair-val-cell))
        ))
    vals
    ))

(defun plist-opts/remove--except-head--no-return (plo key)
  "Auxillary function for plist-opts/remove.
Like plist-opts/remove--except-head, but always returns nil."
  (let* ((prv-pair-val-cell (cdr plo))
         (cur-pair-key-cell (cdr prv-pair-val-cell))
        )
    (while (consp cur-pair-key-cell)
      (if (eq key (car cur-pair-key-cell))
          (progn
            (setq cur-pair-key-cell (cddr cur-pair-key-cell))
            (setcdr prv-pair-val-cell cur-pair-key-cell)
            )
        (setq cur-pair-key-cell (cddr cur-pair-key-cell))
        (setq prv-pair-val-cell (cddr prv-pair-val-cell)
              cur-pair-key-cell (cdr prv-pair-val-cell))
        ))
    ))



(cl-defun plist-opts/assert-well-formed (plo &optional (msg ""))
  "Return true iff putative plist PLO consists of pairs,
where the first element of each pair is a key,
e.g. a symbol with symbol name staring with \":\".

When given, MSG is prepended to any error messages."
  (if (not plo)
      t
    (let1  key  (car plo)
      (if (symbolp key)
          (if (eq ?: (aref (symbol-name key) 0))
              (if (consp (cdr plo))
                  (plist-opts/assert-well-formed (cddr plo) msg)
                (error "%sodd number of elements in putative plist" msg)
                )
            (error "%skey name '%s' does not start with ':'" msg (symbol-name key))
            )
        (error "%skey %S is not a symbol" msg key)
        )
    )
    t
    ))



(provide 'plist-opts)

;;; plist-opts.el ends here
